
const boulier = [[true,true,true,false,false,false], [true,true,true,false,false,false]];

move(0, 1);
print();
move(0, 0);
move(1, 2);
print();
move(1, 0);
print();
move(0, 4);
move(1, 5);
print();

function move(line, col) {
    if (boulier.length <= line || boulier[line].length <= col || ! boulier[line][col]) {
        return false;
    }
    let data = boulier[line]

    // Détermine si il faut bouger les boules vers la gauche ou la droite
    let right = true;
    for (let i = col; i < data.length; i++) {
        if (!data[i]) {
            right = false;
            break;
        }
    }

    // Si a droite inverse la ligne
    if (right) {
        data = data.reverse();
        col = data.length - col - 1;
    }

    data = makemove(data, col);

    // Si a droite remet la ligne
    if (right) {
        data = data.reverse();
    }
}

function makemove(data, col) {
    // Compte le nombre de boule à deplacer et l'index pour faire le déplacement
    let cpt = 0;
    let gap = false;
    let index = null;
    for (let i = col; i < data.length; i++) {
        if (!gap && data[i]) {
            cpt++;
            data[i] = false;
        } else if (gap && data[i]) {
            index = i - 1;
            break;
        } else {
            gap = true;
        }
    }
    if (index == null) {
        index = data.length - 1;
    }

    // Déplace les boules
    for (let i = 0; i < cpt; i++) {
        data[index - i] = true;
    }

    return data;
}

function print() {
    let str = '';
    for (const line of boulier) {
        str += '-|';
        for (const boule of line) {
            if (boule) {
                str += 'o';
            } else {
                str += '-';
            }
        }
        str += '|-\n';
    }
    console.log(str);
}